package com.example.demo.services;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.controllers.UserRestControllerTest;
import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
	
	private static final Logger log = Logger.getLogger(UserServiceTest.class.getName());
	@Autowired
	UserService userService;
	@MockBean
	UserRepository userRepository;
	
	@Before
	public void setup() {}
	
	@Test
	public void getUserByNameTest() {
		User user = new User("Jane","abc","1234");
		Mockito.when(userRepository.findByName("John")).thenReturn(user);
		Assert.assertEquals("1234444", userService.getUserByName("John").password); 
	}
}

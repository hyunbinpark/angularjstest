package com.example.demo;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;


import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.UserService;



@RunWith(SpringRunner.class)
@SpringBootTest
public class UserIntegrationTest {
	private static final Logger log = Logger.getLogger(UserIntegrationTest.class.getName());
	RestTemplate restTemplate;
	@Autowired
	UserService userservice;
	@Autowired
	UserRepository userRepository;
	@Test
	public void userRestControllerTest() {
		//@Value
		String url = "http://localhost:8080/users";
		User[] usersX = restTemplate.getForObject(url, User[].class); 
		List<User> users = Arrays.asList(usersX);
		log.info(users.toString());
		List<User> users2 = userservice.getAllUser();
		Assert.assertEquals(users.size(),users2.size());
		
	}
	@Test
	public void userServiceTest() {
		
	
	}

}

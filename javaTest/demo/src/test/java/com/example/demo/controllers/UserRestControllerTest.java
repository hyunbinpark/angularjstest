package com.example.demo.controllers;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.core.Is;
import org.hamcrest.core.IsEqual;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.rest.webmvc.support.BaseUriLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.apache.log4j.Logger;
import org.codehaus.groovy.transform.EqualsAndHashCodeASTTransformation;

import com.example.demo.models.User;
import com.example.demo.services.UserService;

//resttemplate 사용해서 실제 들어온 제이슨 리턴값 객체 확인 
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRestControllerTest {
	private static final Logger log = Logger.getLogger(UserRestControllerTest.class.getName());
	@Autowired
	UserService userservice;
	RestTemplate resttemplate;
	@Before 
	public void setUp() {}
	
	@Test
	public void testGetUser() throws Exception{ 
		TestRestTemplate testRestTemplate = new TestRestTemplate();
		log.info("**********************resttemplate Test start***************************");
		List<User> resultUser = Arrays.asList(resttemplate.getForObject("http://localhost:8080/api/repository", User.class));
		List<User> users = userservice.getAllUser();
		//ResponseEntity<String> response = testRestTemplate.getForEntity("http://localhost:8080/api/repository", String.class);
		
		Assert.assertEquals(resultUser.size(),users.size());
		
	}
	
}

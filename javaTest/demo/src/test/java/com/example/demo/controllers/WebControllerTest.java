package com.example.demo.controllers;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.models.User;
import com.example.demo.services.UserService;




//서비스를 mockbean으로 해서 스트링으로 리턴값이 메소드랑 같은지 확 
@RunWith(SpringRunner.class)
@SpringBootTest
public class WebControllerTest {
	private static final Logger log = Logger.getLogger(WebControllerTest.class.getName());
	
	@MockBean
	Model model;
	@MockBean
	UserService userService;
	@Autowired
	WebController webcontroller;
	
	@Before
	public void setUp() {
		
	}
	
	@Test
	public void alllistTest() {
		List<User> users = null;
		Mockito.when(userService.getAllUser()).thenReturn(users);
		log.info("**************webController Test Start**********");
		Assert.assertEquals("alllist"/*"list"*/, webcontroller.alllist(model));
	//list >> 	실패 케이스 
	//alllist >> 성공 케이스 
	}
}

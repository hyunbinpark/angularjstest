package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.User;
import com.example.demo.services.UserService;

@RestController
@RequestMapping("users")
public class UserRestController {
	@Autowired
	UserService userservice;
	
	@RequestMapping(method = RequestMethod.GET)
	List<User> getAllUsers() {
		List<User> users = userservice.getAllUser();
		return users;
	}
	@RequestMapping(value="{name}", method = RequestMethod.GET)
	User getUser(@PathVariable String name) {
		User user = userservice.getUserByName(name);
		return user;
	}
	
	
}

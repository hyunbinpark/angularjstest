package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.models.User;
import com.example.demo.services.UserService;

@Controller
@RequestMapping("user")
public class WebController {
	@Autowired
	UserService userservice;
	
	@RequestMapping("alluser")
	public String alllist(Model model) {
		List<User> users =userservice.getAllUser();
		 model.addAttribute("Users",users);
		return "alllist";
	}
	
	
	

}

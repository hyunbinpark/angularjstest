package com.example.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.example.demo.models.User;

@RepositoryRestResource
@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	public User findById(long id);
	public User findByName(String name);
	public List<User> findAll();
	
}

package com.example.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	@Column
	public String name;
	@Column
	public String userid;
	@Column
	public String password;
	
	public User() {}
	public User(String name, String userid, String password) {
		this.name=name;
		this.userid=userid;
		this.password= password;
	}

}

package com.example.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	public List<User> getAllUser() {
		List<User> users = userRepository.findAll();
		return users;
	}
	
	public User getUserByName(String name) {
		
		User user = userRepository.findByName(name);
		
		
		return user;
	}
}
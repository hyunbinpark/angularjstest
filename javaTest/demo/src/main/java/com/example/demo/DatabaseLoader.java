package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;

@Component
public class DatabaseLoader implements ApplicationListener<ContextRefreshedEvent> {
	
	@Autowired
	UserRepository userRepo;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		// TODO Auto-generated method stub
		User user1 =new User();
		user1.name = "John";
		user1.password="1234";
		user1.userid = "Johnsid";
		
		userRepo.save(user1);
		userRepo.save(new User("철수","1234","철수아이디"));
		
	}
	
}

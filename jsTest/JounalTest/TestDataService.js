var DataService = require('./DataService/DataService.js');

function testDataService(){
  console.log("TestDataService start ....");
  var http = {
    get : function(url){
      return new Promise(
        function(resolve, reject){
          resolve(""+url+": i am happy");
        }
      );
    }
  };
  var rootScope = {};
  var Logger = {};
  var subs = {};
  var ds = {};

  this.init = function(){
    console.log("init");
    ds = new DataService(http, rootScope, Logger, subs);
    console.log("init_completed");
  };
  init();

  if (ds != null){
    var promise = ds.getTitleByDate();
    promise.then(
      function(response){
        console.log(response);
      }, function(error){
        console.log(error);
      }
    );
  } else {
    console.log("ds is null");
  }
}

function testDataService02(){
  var axios = require('axios');
  var rootScope = {};
  var Logger = {};
  var subs = {};
  var ds;

  this.init = function(){
    console.log("init");
    ds = new DataService(axios, rootScope, Logger, subs);
    console.log("init_completed");
  };
  init();
console.log("test 2.1 is now starting....");
ds.getCommentsByDate("20170817").then(
  function(response){ console.log("test getCommentsByDate is success" );},
  function(error){
    console.log("printing error");
    console.log(error);
  }
);
 console.log("test 2.2 is now starting ....");
 ds.getNextDate("20170817").then(
   function(response){console.log("success test2.2")}, function(error){console.log("error");}
 );

//console.log("test2.3 broadcasting is now starting");
//ds.testBroadcast("hihi");
}

testDataService02();

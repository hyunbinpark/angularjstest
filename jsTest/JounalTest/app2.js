'use strict';

var angular = require('angular');
var FakeDataService = require('./FakeDataService');
var MyController1 = require('./MyController1');


var app = angular.module('myapp', []);
app.service('DataService',FakeDataService);
app.controller('MyController',MyController1);

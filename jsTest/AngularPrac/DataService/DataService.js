function DataService($http) {


console.log("DataService run");

this.getData01 = function(){
  var data = {'fruit':'apple'};
  return new Promise(function(resolve, reject){
    var response = data;
    resolve(response);
    console.log("success data is " +response.fruit);
  }, function(response){
    reject(response);
  });
}

  this.getData = function() {
    var url = '/data.json';
    console.log("getData run");

    return new Promise(function(resolve, reject){
      console.log("promise run -1");
      $http.get(url).then(function(response){
        console.log("promise run 2 success ");
        resolve(response);
      },function(error){
        console.log("promise run 2 fail");
        reject("errored");
      });
    });

  }
}
module.exports = DataService;

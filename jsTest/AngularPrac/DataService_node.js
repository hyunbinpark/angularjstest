var DataService = require('./DataService/DataService');
var axios = require('axios');
var ds = new DataService(axios);

function TestDataService01(){
  console.log('test1 run -');
  ds.getData01().then(function(result){
    console.log(" test 1 result : " + result.fruit);
  }, function(error){
    console.log("error : " + error);
  });
}
function TestDataService02(){
  console.log('test2 run --');
  ds.getData().then(function(result){
    console.log("---test2 result---");
    console.log("his name is " +result.data.name);
    console.log("his team is " + result.data.team);
  }, function(error){
    console.log(error);
  })
}


TestDataService01();
TestDataService02();

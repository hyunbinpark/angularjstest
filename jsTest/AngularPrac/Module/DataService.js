var util1 = require('./util1');
var events = require('events');
function DataService($http, $rootScope, Logger, subs){
  var temp = new util1();

  var cache = {
    comments: "abc",
    getComments: function(){
      return new Promise(function(resolve,reject){
        resolve(comments);
      }, function(error){
        console.log("error");
      });
    }
  };

/*  var rootBroadcast = function(eventName, data){
    $rootScope.$broadcast(eventName, data);
  };
    노드에서 테스트 할때는 아래와 같이 이벤트를 require 해서 사용한다.
*/

var eventEmitter = new events.EventEmitter();
var rootBroadcast = function(eventName, data){
  console.log("eventName :  "+eventName +" data : "+ data);
}
eventEmitter.on('rootBroadcast', rootBroadcast);

  var url = "http://127.0.0.1:8000/text.json";
  var innerPromise = function(){
    return $http.get(url);
  };

  this.getData = function(){
    var inPromise = innerPromise();
    return new Promise(function(resolve,reject){
      inPromise.then(function(response){
        console.log("i have response");
        resolve(response);
      }, function(error){
        console.log("i don't have response");
        reject(error);
      });
    });
  };

  this.testBroadcast = function(msg){
    console.log("testBroadcast 01");
    var inPromise = innerPromise();
    var promise = new Promise(function(resolve, reject){
      resolve(msg);
      console.log("testBroadcast 02");
      console.log("msg is :" + msg);

    });
    inPromise.then(function(response){
      console.log("testBroadcast 03");
      rootBroadcast('comments-updated', response.data.title);

    }, function(error){
        console.log("error broadcast");
      }
    );
    return promise;
  };
}

module.exports = DataService;

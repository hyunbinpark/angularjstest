

describe("broadcast test ", function() {

  var rootScope;

  beforeEach(inject(function($injector) {
    rootScope = $injector.get('$rootScope');
    spyOn(rootScope,'$broadcast');
  }));


  it("should broadcast something", function(){

    expect(rootScope.$broadcast).toHaveBeenCallWith('comments-updated');

  });
});

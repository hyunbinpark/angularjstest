function MyController($scope, DataService) {
console.log("myController run");

this.setDatafromScope01 = function(){
  console.log("setDatafromScope01 run");


  DataService.getData01().then(function(response){
    console.log("********getData01 test run *********");
    console.log("response :"+ response.fruit);

    $scope.result02 = response.fruit;

    $scope.$apply(function(){
      $scope.result02=response.fruit;
      console.log("scope.apply.....result:"+$scope.result02);
       });
  });
 }


  this.setDatafromScope = function() {

      console.log("setDatafromScope run");

    DataService.getData().then(function(response){
      console.log("response :"+ response);
      $scope.result = response;
      console.log("scope.result :" + $scope.result.name);

      $scope.$applyAsync(function(){
          //if ($scope.$$phase == '$apply' || $scope.$$phase == '$digest') {
            $scope.result01=response;
            console.log("scope.apply.....result:"+$scope.result01);
          //}
      });

    }, function(error){
      console.log("error :" +error);
    });

  }

$scope.setDatafromScope01 = this.setDatafromScope01;
$scope.setDatafromScope = this.setDatafromScope;

}
module.exports= MyController;

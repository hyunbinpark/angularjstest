
var div1 = document.getElementById("div1");
var div2 = document.getElementById("div2");
var div3 = document.getElementById("div3");

var positions =[0,1,2];  //div1, div2, div3
var slide_animation = [0,0,0];
var posClassName ={"0":"left", "1":"centre", "2":"right"};  //dictionary
var slideSelect = {"0":"","1":"slide-animate"};
var i;
function mvLeft(){
//$("#container").fadeOut(1);
console.log('mvLeft();');
shiftleft();
console.log("mvLeft run");


console.log("[div1, div2, div3]");
console.log("pos: ["+ positions+"]");
for(i=0;i<slide_animation.length;i++){

if(positions[i]==2){
  slide_animation[i] = 0;
}else{
slide_animation[i] = 1;
  }
}
console.log("animations : ["+slide_animation+"]");
console.log("positions after slide : " + positions);
div1.className = posClassName[positions[0]]+" "+slideSelect[slide_animation[0]];
div2.className = posClassName[positions[1]]+" "+slideSelect[slide_animation[1]];
div3.className = posClassName[positions[2]]+" "+slideSelect[slide_animation[2]];

//$("#container").delay(500).fadeIn(500);

// 방법1
// animation
//$("#curtain").animate({

//});

// 방법2

// $("#container").addClass("hide");
//
// setTimeout(function() {
//   $("#container").removeClass("hide");
// }, 500);

}
function mvRight(){
shiftright();
for(i=0;i<slide_animation.length;i++){

if(positions[i]==0){
  slide_animation[i] = 0;
}else{
slide_animation[i] = 1;
  }
}

div1.className = posClassName[positions[0]]+" "+slideSelect[slide_animation[0]]; //'left slide-animate'
div2.className = posClassName[positions[1]]+" "+slideSelect[slide_animation[1]];
div3.className = posClassName[positions[2]]+" "+slideSelect[slide_animation[2]];

//$("#container").addClass("hide");

//setTimeout(function() {
//  $("#container").removeClass("hide");
//}, 500);

}
function shiftleft(){
  for(i=0;i<positions.length;i++){
    if(positions[i]<=0){
      positions[i] = 2;

    }else{
      positions[i] = positions[i]-1;

    }
  }

}
function shiftright(){
  for(i=0;i<positions.length;i++){
    if(positions[i]>=2){
      positions[i] = 0;
    }else{
      positions[i] = positions[i]+1;
    }
  }
}

/* 두가지 방법으로 구현
1. jquery지원함수  fadeIn fadeOut
2. container와 container hide 클래스를미리 만들어 놓고 버튼 클릭시 hide 가 붙었다가 일정시간
후에 사라지는 식으로 하고 container hide 클래스를 opacity를 0을 주어서 투명하게 나타나게 하였다.
ㄴ*/

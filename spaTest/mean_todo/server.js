// server.js
	// set up ========================
	var express  = require('express');
	var app      = express(); 			// create our app w/ express
	var mongoose = require('mongoose'); // mongoose for mongodb

	// configuration =================
	// connect to mongoDB database on modulus.io
	mongoose.connect('mongodb://localhost/todo'); 	

	app.configure(function() {
		// set the static files location /public/img will be /img for users
		app.use(express.static(__dirname + '/public'));
		// log every request to the console
		app.use(express.logger('dev'));
		// pull information from html in POST
		app.use(express.bodyParser());
		// simulate DELETE and PUT
		app.use(express.methodOverride());
	});
	// listen (start app with node server.js) ======================================
	app.listen(8080);
	console.log("App listening on port 8080");

